package com.example.restapitest.controllers;

import com.example.restapitest.models.Clinic;
import com.example.restapitest.retrofit.api.ApiService;
import com.example.restapitest.retrofit.api.RetroClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClinicController {
    ApiService api;

    public ArrayList<Clinic.ItemClinic.Cells> getClinics() {
        ArrayList<Clinic.ItemClinic.Cells> mClinicList = new ArrayList<>();

        //Creating an object of our api interface
        api = RetroClient.getApiService(RetroClient.ROOT_URL_OPEN_DATA);

        //TODO исправить url для routes
        Call<Clinic> call = api.getClinicsJSON("data/?id=1935588");
        call.enqueue(new Callback<Clinic>() {
            @Override
            public void onResponse(@NotNull Call<Clinic> call, @NotNull Response<Clinic> response) {
                Clinic clinic = response.body();

                assert clinic != null;
                for (Clinic.ItemClinic itemClinic : clinic.itemClinic) {
                    String nameCompany = itemClinic.cells.nameCompany;
                    String services = itemClinic.cells.services;

                    mClinicList.add(new Clinic.ItemClinic.Cells(nameCompany, services));
                }
            }

            @Override
            public void onFailure(Call<Clinic> call, Throwable t) {
            }
        });

        return mClinicList;
    }
}
