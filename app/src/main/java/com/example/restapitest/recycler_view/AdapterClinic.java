package com.example.restapitest.recycler_view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.restapitest.R;
import com.example.restapitest.models.Clinic;
import com.example.restapitest.models.Item;
import com.example.restapitest.ui.activity.RecyclerViewClinic;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterClinic  extends RecyclerView.Adapter<AdapterClinic.ViewHolder> {
    private Context mContext;
    private ArrayList<Clinic.ItemClinic.Cells> mClinicList;
    private RecyclerViewClinic mListener;

    public interface OnItemClickListener {
        void OnItemClick(int position);
    }

    public void setOnItemClickListener(RecyclerViewClinic listener) {
        mListener = listener;
    }

    public AdapterClinic(Context context, ArrayList<Clinic.ItemClinic.Cells> clinicList) {
        mContext = context;
        mClinicList = clinicList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.card_view_item_clinic, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Clinic.ItemClinic.Cells currentItem = mClinicList.get(position);

        String company_name = currentItem.nameCompany;
        String services = currentItem.services;

        holder.mNameCompany.setText(company_name);
        holder.mServices.setText(services);
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mNameCompany;
        public TextView mServices;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mNameCompany = itemView.findViewById(R.id.tv_name_company);
            mServices = itemView.findViewById(R.id.tv_services);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mListener.OnItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
