package com.example.restapitest.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Clinic {
    @SerializedName("name")
    public String name;
    @SerializedName("totalCount")
    public int totalCount;
    @SerializedName("Items")
    public ArrayList<ItemClinic> itemClinic = null;

    public static class ItemClinic {
        @SerializedName("Number")
        public int number;
        @SerializedName("Cells")
        public Cells cells;

        public static class Cells {
            public Cells(String nameCompany, String services) {
                this.nameCompany = nameCompany;
                this.services = services;
            }

            @SerializedName("NAME_OF_COMPANY")
            public String nameCompany;
            @SerializedName("SERVICES")
            public String services;
            @SerializedName("STREET")
            public String street;
            @SerializedName("HOUSE")
            public String house;
            @SerializedName("MODE")
            public String mode;
            @SerializedName("SITE")
            public String site;
            @SerializedName("GEOOBJECT_NAME")
            public String geoName;
            @SerializedName("GEOCOORD")
            public String geoCoord;
        }
    }


}

