package com.example.restapitest.retrofit.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClient {
    /**
     * URLS
     */
    public static final String ROOT_URL_OPEN_DATA = "https://data.admhmao.ru/api/";
    public static final String ROOT_URL_FAKE_DATA = "https://jsonplaceholder.typicode.com/";

    /**
     * Get Retrofit Instance
     */
    private static Retrofit getRetrofitInstance(String url) {
        Gson gson = new GsonBuilder().serializeNulls().create();

        //Логирование запросов
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();

        return new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create()) //gson можно передать сюда
                .client(okHttpClient)
                .build();
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static ApiService getApiService(String url) {
        return getRetrofitInstance(url).create(ApiService.class);
    }
}
