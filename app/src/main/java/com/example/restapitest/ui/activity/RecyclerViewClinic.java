package com.example.restapitest.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.restapitest.R;
import com.example.restapitest.controllers.ClinicController;
import com.example.restapitest.models.Clinic;
import com.example.restapitest.models.Item;
import com.example.restapitest.recycler_view.AdapterClinic;
import com.example.restapitest.recycler_view.AdapterJson;

import java.util.ArrayList;

public class RecyclerViewClinic extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private AdapterClinic mAdapterClinic;
    ArrayList<Clinic.ItemClinic.Cells> mClinicList = new ArrayList<>();
    ClinicController clinicController = new ClinicController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_clinic);

        mClinicList = clinicController.getClinics();

        mAdapterClinic = new AdapterClinic(RecyclerViewClinic.this, mClinicList);
        mRecyclerView.setAdapter(mAdapterClinic);
        mAdapterClinic.setOnItemClickListener(RecyclerViewClinic.this);
    }


    public void OnItemClick(int position) {

    }
}