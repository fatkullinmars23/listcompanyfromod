package com.example.restapitest.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.restapitest.R;
import com.example.restapitest.models.Clinic;
import com.example.restapitest.retrofit.api.ApiService;
import com.example.restapitest.retrofit.api.RetroClient;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClinicActivity extends AppCompatActivity {
    //TODO добавить RecyclerView
    ApiService api;
    private TextView textViewResultMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic);

        textViewResultMain = findViewById(R.id.text_view_result_main);

        //Creating an object of our api interface
        api = RetroClient.getApiService(RetroClient.ROOT_URL_OPEN_DATA);

        //получить список ветеринарок
        getClinics();
    }

    private void getClinics() {
        //TODO исправить url для routes
        Call<Clinic> call = api.getClinicsJSON("data/?id=1935588");
        call.enqueue(new Callback<Clinic>() {
            @Override
            public void onResponse(@NotNull Call<Clinic> call, @NotNull Response<Clinic> response) {
                String content = "";
                Clinic clinic = response.body();

                //формирование переменных с данными от сервера
                assert clinic != null;
                String name = clinic.name;
                int total = clinic.totalCount;
                List<Clinic.ItemClinic> itemClinicList = clinic.itemClinic;

                content += "Name: " + name + "\n";
                content += "Total count: " + total + "\n";
                for (Clinic.ItemClinic itemClinic : itemClinicList) {
                    content += "Number: " + itemClinic.number + "\n";
                    content += "Name of Company: " + itemClinic.cells.nameCompany + "\n";
                    content += "Mode: " + itemClinic.cells.mode + "\n";
                }

                textViewResultMain.setText(content);
            }

            @Override
            public void onFailure(Call<Clinic> call, Throwable t) {
                textViewResultMain.setText(t.getMessage());
            }
        });
    }
}