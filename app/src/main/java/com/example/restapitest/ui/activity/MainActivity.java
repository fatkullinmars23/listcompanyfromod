package com.example.restapitest.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.restapitest.R;
import com.example.restapitest.models.Clinic;
import com.example.restapitest.retrofit.api.ApiService;
import com.example.restapitest.retrofit.api.RetroClient;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_posts = findViewById(R.id.btn_posts);
        //тоже самое, что и new OnClickListener для кнопки
        Button btn_clinics = findViewById(R.id.btn_clinics);
        Button btn_recycler_view_json = findViewById(R.id.btn_recycler_view_json);

        btn_posts.setOnClickListener(view -> goToPostsActivity());
        btn_clinics.setOnClickListener(view -> goToClinicsActivity());
        btn_recycler_view_json.setOnClickListener(view -> goToRecyclerViewJsonActivity());
    }

    private void goToPostsActivity() {
        Intent intent = new Intent(MainActivity.this, PostRequests.class);
        startActivity(intent);
    }

    private void goToClinicsActivity() {
        Intent intent = new Intent(MainActivity.this, ClinicActivity.class);
        startActivity(intent);
    }

    private void goToRecyclerViewJsonActivity() {
        Intent intent = new Intent(MainActivity.this, RecyclerViewJsonActivity.class);
        startActivity(intent);
    }
}